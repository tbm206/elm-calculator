-- Read all about this program in the official Elm guide:
-- https://guide.elm-lang.org/architecture/user_input/text_fields.html

import Html exposing (Html, Attribute, beginnerProgram, text, div, p, button)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import String
import Debug exposing (log)


main =
  beginnerProgram { model = model, view = view, update = update }

-- MODEL

model = {expression = [], result = 0.0}

-- Types
type Number
  = Zero
  | One
  | Two
  | Three
  | Four
  | Five
  | Six
  | Seven
  | Eight
  | Nine

type Operation
  = Add
  | Subtract
  | Multiply
  | Divide

type Symbol
  = Digit Number
  | Arithmetic Operation
  | Dot

type alias Expression = List Symbol

type Msg
  = Formulate Symbol
  | Calculate
  | Clear

-- UPDATE

-- Stringifying
mapOperationToString : Operation -> String
mapOperationToString operation =
  case operation of
    Add -> "+"
    Subtract -> "-"
    Multiply -> "x"
    Divide -> "/"

mapDigitToString : Number -> String
mapDigitToString digit =
  case digit of
    Zero -> "0"
    One -> "1"
    Two -> "2"
    Three -> "3"
    Four -> "4"
    Five -> "5"
    Six -> "6"
    Seven -> "7"
    Eight -> "8"
    Nine -> "9"

mapSymbolToString : Symbol -> String
mapSymbolToString symbol =
  case symbol of
    Arithmetic operation -> mapOperationToString operation
    Digit number -> mapDigitToString number
    Dot -> "."

stringifyExpression : Expression -> String
stringifyExpression expression =
  List.map mapSymbolToString expression |> String.concat

-- Computation
getNumbers : Expression -> List Float
getNumbers expression =
  expression
  |> List.foldr
    (\symbol acc ->
      case acc of
        [] ->
          case symbol of
            Digit n -> [mapDigitToString n]
            _ -> []
        [a] ->
          case symbol of
            Arithmetic _ -> ["", a]
            Digit n -> [a ++ (mapDigitToString n)]
            Dot -> [a ++ "."]
        hd::tl ->
          case symbol of
            Arithmetic _ -> ""::acc
            Digit n -> (hd ++ (mapDigitToString n))::tl
            Dot -> (hd ++ ".")::tl
    )
    []
  |> List.map String.toFloat
  |> List.map
    (\result ->
      case result of
        Ok value -> value
        Err _ -> 0.0
    )

getOperations : Expression -> Expression
getOperations expression =
  List.append
    (List.filter
      (\a ->
        case a of
          Arithmetic _ -> True
          _ -> False
      )
      expression
    )
    [Arithmetic(Add)]

selectAdditonAndSubtraction : Expression -> Expression
selectAdditonAndSubtraction expression =
  List.append
    (List.filter
      (\symbol ->
        case symbol of
          Arithmetic Add -> True
          Arithmetic Subtract -> True
          _ -> False
      )
      expression
    )
    [Arithmetic(Multiply)]

exhaustMultiplicationAndDivision : Expression -> List Float
exhaustMultiplicationAndDivision expression =
  let
    mergedDigitAndOperation = List.map2 (,) (getNumbers expression) (getOperations expression)
  in
    List.foldr
      (\element acc ->
        let
          (digit, operation) = element
        in
          case acc of
            [] -> [digit]
            [a] ->
              case operation of
                Arithmetic Multiply -> [(*) a digit]
                Arithmetic Divide -> [(/) a digit]
                _ -> [digit, a]
            hd::tl ->
              case operation of
                Arithmetic Multiply -> ((*) hd digit)::tl
                Arithmetic Divide -> ((/) hd digit)::tl
                _ -> digit::acc
      )
      []
      mergedDigitAndOperation

exhaustAddtionAndSubtraction : Expression -> List Float -> Float
exhaustAddtionAndSubtraction expression multiplied =
  let
    mergedFloatAndOperation = List.map2 (,) multiplied (selectAdditonAndSubtraction expression)
  in
    List.foldr
    (\element acc ->
      let
        (float, operation) = element
      in
        case operation of
          Arithmetic Add -> ((+) acc float)
          Arithmetic Subtract -> ((-) acc float)
          _ -> float
    )
    0.0
    mergedFloatAndOperation

willZeroBeLeading : Expression -> Bool
willZeroBeLeading expression =
  case expression of
    [] -> True
    hd::_ ->
      case hd of
        Arithmetic _ -> True
        _ -> False

hasDotPreceded : Expression -> Bool
hasDotPreceded expression =
  case expression of
    [] -> False
    [symbol] ->
      case symbol of
        Dot -> True
        _ -> False
    hd::tl ->
      case hd of
        Dot -> True
        Arithmetic _ -> False
        Digit _ -> hasDotPreceded tl

removeLeadingOperation : Expression -> Expression
removeLeadingOperation expression =
  case expression of
    [] -> []
    hd::tl ->
      case hd of
        Arithmetic _ -> tl
        _ -> expression

computeResult : Expression -> Float
computeResult expression =
  exhaustAddtionAndSubtraction expression (exhaustMultiplicationAndDivision expression)

-- Update Expression
update msg {expression, result} =
  case msg of
    Calculate -> {expression = expression, result = expression |> removeLeadingOperation |> computeResult}
    Clear -> {expression = [], result = 0.0}
    Formulate symbol ->
      {expression =
        case symbol of
          Digit digit ->
            case digit of
              Zero ->
                case (willZeroBeLeading expression) of
                  True -> expression
                  False -> symbol::expression
              _ -> symbol::expression
          Dot ->
            case (hasDotPreceded expression) of
              True -> expression
              False -> symbol::expression
          Arithmetic _ ->
            case expression of
              [] -> []
              [_] -> symbol::expression
              hd::tl ->
                case hd of
                  Digit _ -> symbol::expression
                  Arithmetic _ -> symbol::tl
                  Dot -> expression
      , result = result}


-- VIEW

view {expression, result} =
  div [ class "calculator"]
    [ div [ class "expression" ]
      [ p [] [ text (List.reverse expression |> stringifyExpression) ]
      , div [ class "result" ]
        [ p [] [ text (toString result)] ]
      ]
    , button [ class "ac", onClick Clear ] [ text "AC"]
    , button [ class "divide", onClick (Formulate(Arithmetic(Divide)))] [ text "/"]
    , button [ class "multiply", onClick (Formulate(Arithmetic(Multiply)))] [ text "X"]
    , button [ class "subtract", onClick (Formulate(Arithmetic(Subtract)))] [ text "-"]
    , button [ class "add", onClick (Formulate(Arithmetic(Add)))] [ text "+"]
    , button [ class "equals", onClick Calculate] [ text "="]
    , button [ class "dot", onClick (Formulate(Dot))] [ text "."]
    , button [ class "zero", onClick (Formulate(Digit(Zero)))] [ text "0"]
    , button [ class "one", onClick (Formulate(Digit(One)))] [ text "1"]
    , button [ class "two", onClick (Formulate(Digit(Two)))] [ text "2"]
    , button [ class "three", onClick (Formulate(Digit(Three)))] [ text "3"]
    , button [ class "four", onClick (Formulate(Digit(Four)))] [ text "4"]
    , button [ class "five", onClick (Formulate(Digit(Five)))] [ text "5"]
    , button [ class "six", onClick (Formulate(Digit(Six)))] [ text "6"]
    , button [ class "seven", onClick (Formulate(Digit(Seven)))] [ text "7"]
    , button [ class "eight", onClick (Formulate(Digit(Eight)))] [ text "8"]
    , button [ class "nine", onClick (Formulate(Digit(Nine)))] [ text "9"]
    ]
